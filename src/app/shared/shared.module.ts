import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './components/footer/footer.component';
import { InputComponent } from './components/input/input.component';
import { ErrorsComponent } from './components/errors/errors.component';

@NgModule({
    declarations: [
        FooterComponent,
        InputComponent,
        ErrorsComponent
    ],
    exports: [
        FooterComponent,
        InputComponent,
        ErrorsComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule
    ]
})

export class SharedModule {}
