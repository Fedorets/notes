import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-wrapper-edit-input',
  templateUrl: './wrapperEditInput.component.html',
  styleUrls: ['./wrapperEditInput.component.scss']
})

export class WrapperEditInputComponent {
  @Input() public label: string;
  @Input() public id: string;
  @Input() public control: FormControl;
  @Input() public type: string = 'text';
  @Input() public placeholder: string;
  @Input() public isEdit: boolean = false;
}
