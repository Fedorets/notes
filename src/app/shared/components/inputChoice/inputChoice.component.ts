import { Component, Input,  ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-input-choice',
  templateUrl: './inputChoice.component.html',
  styleUrls: ['./inputChoice.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InputChoiceComponent {
  @Input() public type: string = 'text';
  @Input() public label: string = null;
  @Input() public id: string;
  @Input() public control: FormControl;
  public even(e) {
    // console.log(e.target.value);
    console.log(this.control.value);
    // console.log(this.control, e.target.value);
  }
}
