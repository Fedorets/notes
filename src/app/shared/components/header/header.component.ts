import { Component, OnDestroy, OnInit } from '@angular/core';

import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'z-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnDestroy, OnInit {
  public user;
  public isLogin: boolean;
  constructor(private auth: AuthService,
              private router: Router) {}
  public logout() {
    this.auth.logout().then((res) => {
      if (res) {
        this.router.navigate(['login']);
      }
    });
  }
  public ngOnInit() {
    this.isLogin = this.auth.isLogin();
  }
  public ngOnDestroy() {
  }
}
