import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})

export class SelectComponent implements OnInit {
  @Input() public label: string;
  @Input() public dataSelect;
  @Input() public placeholder: string;
  @Input() public control: FormControl;
  constructor() {}
  public ngOnInit() {
    // console.log(this.control, this.dataSelect);
  }
}
