import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-wrapper-edit-select',
  templateUrl: './wrapperEditSelect.component.html',
  styleUrls: ['./wrapperEditSelect.component.scss']
})

export class WrapperEditSelectComponent {
  @Input() public label: string;
  @Input() public id: string;
  @Input() public control: FormControl;
  @Input() public type: string = 'text';
  @Input() public placeholder: string;
  @Input() public dataSelect;
  @Input() public isEdit: boolean = false;
}
