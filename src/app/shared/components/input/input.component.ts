
import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'nt-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit {
  @Input() public type = 'text';
  @Input() public label = null;
  @Input() public placeholder = '';
  @Input() public isTextarea = false;
  @Input() public control: FormControl = null;
  public id;

  constructor() {
      this.id = 'input' + (+new Date() * Math.random());
  }
  public ngOnInit() {
    console.log(this.label, this.isTextarea, this.control);
  }
}
