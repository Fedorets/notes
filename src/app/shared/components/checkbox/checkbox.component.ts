import { Component, Input,  ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CheckboxComponent {
  @Input() public label: string = null;
  @Input() public control: FormControl;
  public id: string;
  constructor() {
    this.id = 'checkbox' + (+new Date() * Math.random());
  }
}
