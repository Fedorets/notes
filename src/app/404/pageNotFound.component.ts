import { Component } from '@angular/core';

@Component({
    selector: 'note-error',
    templateUrl: './pageNotFound.component.html',
    styleUrls: ['./pageNotFound.component.scss']
})

export class PageNotFoundComponent {}
