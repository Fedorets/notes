import { Component } from '@angular/core';
import { MdDialog } from '@angular/material';

import { AddOptionsComponent } from '../add-options/add-options.component';

@Component({
    selector: 'nt-add-options-btn',
    templateUrl: './add-options-btn.component.html',
    styleUrls: ['./add-options-btn.component.scss']
})

export class AddOptionsBtnComponent {
    constructor(public dialog: MdDialog) {}
    public openDialog() {
        this.dialog.open(AddOptionsComponent);
    }
}
