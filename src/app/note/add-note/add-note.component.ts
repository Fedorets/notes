import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'nt-add-note',
    templateUrl: './add-note.component.html',
    styleUrls: ['./add-note.component.scss']
})

export class AddNoteComponent {
    public addNoteForm: FormGroup;
    public foods = [
        {value: 'steak-0', viewValue: 'Steak'},
        {value: 'pizza-1', viewValue: 'Pizza'},
        {value: 'tacos-2', viewValue: 'Tacos'}
    ];
    constructor(fb: FormBuilder) {
        this.addNoteForm = fb.group({
            text: [null, Validators.required]
        });
    }
}
