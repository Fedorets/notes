import { Component } from '@angular/core';
import { MdDialog } from '@angular/material';

import { AddNoteComponent } from '../add-note/add-note.component';

@Component({
    selector: 'nt-add-note-btn',
    templateUrl: './add-note-btn.component.html',
    styleUrls: ['./add-note-btn.component.scss']
})

export class AddNoteBtnComponent {
    constructor(public dialog: MdDialog) {}
    public openDialog() {
        this.dialog.open(AddNoteComponent);
    }
}
