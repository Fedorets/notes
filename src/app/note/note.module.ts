import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NotesListComponent } from './notes-list/notes-list.component';
import { NoteItemComponent } from './note-item/note-item.component';
import { AddNoteComponent } from './add-note/add-note.component';
import { SharedModule } from '../shared/shared.module';

import {
    MdButtonModule,
    MdIconModule,
    MdInputModule,
    MdSelectModule,
    MdChipsModule,
    MdDialogModule,
    MdTabsModule, MdCoreModule
} from '@angular/material';
import { AddOptionsComponent } from './add-options/add-options.component';
import { AddOptionsBtnComponent } from './add-options-btn/add-options-btn.component';
import { AddNoteBtnComponent } from './add-note-btn/add-note-btn.component';

@NgModule({
    declarations: [
        NotesListComponent,
        NoteItemComponent,
        AddNoteComponent,
        AddOptionsComponent,
        AddOptionsBtnComponent,
        AddNoteBtnComponent
    ],
    exports: [
        NotesListComponent,
        AddOptionsBtnComponent,
        AddNoteComponent,
        AddOptionsComponent,
        AddNoteBtnComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        CommonModule,
        MdInputModule,
        MdButtonModule,
        MdCoreModule,
        MdIconModule,
        MdSelectModule,
        MdChipsModule,
        MdDialogModule,
        MdTabsModule
    ],
    entryComponents: [
        AddOptionsComponent,
        AddNoteComponent
    ]
})

export class NoteModule {}
