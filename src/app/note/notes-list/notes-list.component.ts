import { Component } from '@angular/core';

@Component({
    selector: 'nt-notes-list',
    templateUrl: './notes-list.component.html',
    styleUrls: ['./notes-list.component.scss']
})

export class NotesListComponent {}
