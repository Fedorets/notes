import { Component } from '@angular/core';

@Component({
    selector: 'nt-note-item',
    templateUrl: './note-item.component.html',
    styleUrls: ['./note-item.component.scss']
})

export class NoteItemComponent {}
